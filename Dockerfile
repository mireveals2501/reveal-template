FROM alpine:latest

# Récupération de la dernière version de reveal.js
RUN wget -O reveal.zip https://github.com/hakimel/reveal.js/archive/master.zip
RUN wget -O menu.zip  https://github.com/denehyg/reveal.js-menu/archive/master.zip
RUN wget -O pluginsplus.zip https://github.com/rajgoel/reveal.js-plugins/archive/master.zip

# Extraction de l'archive
RUN unzip reveal.zip -d reveal
RUN unzip menu.zip -d menu
RUN unzip pluginsplus.zip -d pluginsplus
# Création d'un dossier public pour stocker notre future présentation
RUN mkdir /public


# Copie des assets nécessaires reveal.js et menu customcontrols test
RUN mv menu/reveal.js-menu-master reveal/reveal.js-master/plugin
RUN mv pluginsplus/reveal.js-plugins-master reveal/reveal.js-master/plugin
RUN mv reveal/reveal.js-master/dist /public/
RUN mv reveal/reveal.js-master/plugin /public/


# Copie du fichier index.html modifié (gestion du markdown, et lien vers le fichier css custom)
COPY ./index.html /public/

# Copie des styles custom et img folder
RUN mkdir /public/custom
RUN mkdir /public/img
COPY ./custom.css /public/custom/
COPY img /public/img/
RUN ls /public/dist/theme